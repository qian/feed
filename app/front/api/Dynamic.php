<?php

namespace app\front\api;

use app\feeService\rpcService\FeedInterface;
use app\front\lib\Db;
use app\front\model\DynamicModel;

/**
 * @method
 * @method rpc($server, $service, $method, $args)
 * @property int uid
 */
class Dynamic
{

    /**
     * 增加动态
     * @return array
     */
    public function add()
    {
        $post = $this->request->post();
        $post['uid'] = $this->uid;
        $model = new Dynamic();
        Db::begin();
        try {
            $rs = $model->save($post);
            //todo 其他相关逻辑
            trigger('user.dynamic.add', [
                'id' => $model->id,
                'uid' => $this->uid,
            ]);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            //todo 其他错误处理逻辑
            $rs = false;
        }

        if (!$rs) {
            return [
                'code' => 500,
                'msg' => 'fail'
            ];
        }
        return [
            'code' => 200,
            'msg' => 'success'
        ];
    }

    /**
     * 获取动态
     * @return array
     */
    public function get()
    {
        $dynamicIds = $this->rpc(
            'feedService',
            FeedInterface::class)->get([
            $this->uid,
            $this->request->get('page')
        ]);
        $rs = [];
        if ($dynamicIds) {
            $rs = DynamicModel::wehreIn('id', $dynamicIds)->find();
        }

        return [
            'code' => 200,
            'data' => $rs
        ];
    }

    /**
     * 主动拉去动态
     * @return array
     */
    public function pull()
    {
        $dynamicIds = $this->rpc(
            'feedService',
            FeedInterface::class
        )->pull([
            $this->uid,
            $this->request->get('page')
        ]);
        $rs = [];
        if ($dynamicIds) {
            $rs = DynamicModel::wehreIn('id', $dynamicIds)->find();
        }
        return [
            'code' => 200,
            'data' => $rs
        ];
    }

}