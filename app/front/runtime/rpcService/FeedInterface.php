<?php

namespace app\feeService\rpcService;

interface FeedInterface
{
    /**
     * 增加一条推送
     * @param int $dynamicId
     * @return bool
     */
    public function add(int $dynamicId);

    /**
     * 获取用户推送
     * @param string $uid
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function get(string $uid, $page = 1, $limit = 10);

    /**
     * 从队列拉去
     * @param string $uid
     * @param int $limit
     * @return
     */
    public function pull(string $uid, $limit = 10);
}