<?php


namespace app\front\model;


abstract class Base
{

    /**
     * 单个查询
     * @param $id
     * @return array
     */
    static public function findOne($id)
    {
        return [];
    }

    /**
     * 多条查询
     * @param $id
     * @return array
     */
    static public function find($cond)
    {
        return [];
    }

    /**
     * 新增
     * @param array $data
     * @return bool
     */
    static public function insert(array $data): bool
    {
        return true;
    }

    /**
     * 批量新增
     * @param array $data
     * @return bool
     */
    static public function insertBatch(array $data): bool
    {
        return true;
    }

    /**
     * 更新
     * @param array $data
     * @param array $cond
     * @return bool
     */
    static public function update(array $data, array $cond): bool
    {
        return true;
    }

    /**
     * 返回总数
     * @param array $cond
     * @return int
     */
    static public function count(array $cond)
    {
        return 1000;
    }

    /**
     * 块执行
     * @param int $number
     * @param array $cond
     * @param $function
     */
    static public function chunk(int $number, array $cond, $function)
    {
        //执行回调function, 适用于数据量较打批量分段执行
    }

}