<?php


/**
 * 获取配置
 * @param $key
 * @return string
 */
function config($key)
{
    return '';
}


/**
 * 事件监听
 * @param $name
 * @param $function
 */
function listener($name, $function)
{
    //注册
    static $listeners;
    return $listeners[$name] = $function;
}

/**
 * 事件触发
 * @param $name
 * @param $args
 */
function trigger($name, $args)
{
    static $listeners;
//    return call_user_func_array($listeners[$name], $args);
    return null;
}


function getRedis()
{
    static $redisStore;
    if (!$redisStore) {
        $redisStore = new Redis();
        $redisStore->connect(config('redis.host'), config('redis.port'));
    }
    return $redisStore;
}