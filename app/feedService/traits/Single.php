<?php
namespace app\feedService\traits;

trait Single
{

    protected static $instance;

    /**
     * @return static
     */
    static public function getInstance()
    {
        if (static::$instance) {
            return static::$instance;
        }
        return static::$instance = new static();
    }

}