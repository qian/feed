<?php

namespace app\feedService\logic;

use app\feedService\enum\FeedStatus;
use app\feedService\exceptions\FeedException;
use app\feedService\model\Feed;
use app\feedService\model\Follow;
use app\feedService\model\FeedLog;
use app\feedService\traits\Single;
use Redis;

class FeedLogic
{
    use Single;

    /**
     * 发送推送
     * @param Feed $feed
     * @return bool
     */
    public function send(Feed $feed)
    {
        try {
            if ($feed['expire_time'] > time()) {
                return $feed->setStatus(FeedStatus::FAIL);
            }
            $feed->setStatus(FeedStatus::SENDING);
        } catch (\Exception $e) {
            //todo 推送信息状态修改失败，
            //记录日志
            return false;
        }
        //关注者总数
        $cond = [
            'uid' => $feed['uid']
        ];
        $followTotal = Follow::count($cond);
        $maxPush = config('feed.maxPush');
        $maxPush = $followTotal > $maxPush ? $maxPush : $followTotal;
        $cond = [
            'uid' => $feed['uid'],
            'limit' => $maxPush,
            'order' => ['id', 'desc']
        ];
        //获取关注者列表，批量发送
        Follow::chunk(200, $cond, function ($items) use ($feed) {
            $post = [];
            foreach ($items as $item) {
                $post[] = [
                    'feed_id' => $feed['id'],
                    'dynamic_id' => $feed['dynamic_id'],
                    'send_uid' => $feed['uid'],
                    'receive_uid' => $item['follow_uid'],
                ];
            }
            try {
                FeedLog::insertBatch($post);
            } catch (\Exception $e) {
                //todo 部分feed信息插入异常，记录 发送任务id和发送失败的段，和错误栈，以供排查
            }
        });

        //检测
        $totalSend = FeedLog::count([
            $feed['id']
        ]);
        try {
            if ($totalSend == $maxPush) {
                //发送条数和需要发送记录一致，则更新
                $feed->setStatus(FeedStatus::COMPLETE);
            } else {
                $feed->setStatus(FeedStatus::FAIL);
            }
        } catch (\Exception $e) {
            //todo 信息推流完成，状态更改失败
        }

        if ($followTotal > config('bigVip.followNum')) {
            //如果不是大v,流程到这里就结束
            return true;
        }
        try {
            $feed->setVipStatus(FeedStatus::SENDING);
        } catch (\Exception $e) {
            //todo 推送信息状态修改失败，
            //记录日志
            return false;
        }
        $this->sendBigVip($feed);
    }

    /**
     * 大v流程
     * @param $feed
     * @return bool
     */
    public function sendBigVip($feed)
    {
        //大V流程
        $rs = false;
        try {
            $rs = $this->sendBigVipToRedisList($feed);
        } catch (\Exception $e) {
            //todo 大v 推送队列出了问题，记录日志，
        }
        try{
            $feed->setVipStatus($rs ? FeedStatus::COMPLETE : FeedStatus::FAIL);
        } catch (\Exception $e) {
            //todo 大v 推送队列完成了，状态更改有错误，记录日志
        }
        return $rs;
    }

    /**
     * 大v推送到到关注者队列
     * @param $feed
     * @return bool
     * @throws FeedException
     */
    public function sendBigVipToRedisList($feed)
    {
        $cond = [
            'uid' => $feed['uid'],
            'offset' => 2000,
            'order' => ['id', 'desc']
        ];
        //获取关注者列表，批量发送
        $hasError = false;
        Follow::chunk(200, $cond, function ($items) use ($feed, $hasError) {
            foreach ($items as $item) {
                try {
                    //将推送信息加入各个关注者的推送队列
                    $this->getRedis()->lpush("feed-user:" . $feed['follow_uid'], $feed['dynamic_id']);
                } catch (\Exception $e) {
                    $hasError = true;
                    //todo 记录队列推送失败日志，
                    continue;
                }
                try {
                    //将此用户加入任务已推送队列， 用于统计和分析feed推送情况
                    $this->getRedis()->lpush("feed-list:" . $feed['id'], $item['follow_uid']);
                } catch (\Exception $e) {
                    //todo 记录已推送，但是任务队列增加失败日志，
                    $hasError = true;
                }
            }
        });

        if (!$hasError) {
            throw new FeedException(
                FeedException::$pushVipHasError[0],
                FeedException::$pushVipHasError[1]
            );
        }
        return true;
    }

    protected $redis;

    /**
     * 获取redis队列
     * @return Redis
     */
    protected function getRedis()
    {
        return getRedis();
    }

    /**
     * 获取推送信息
     * @param $feedId
     * @return array
     */
    public function getFeedById($feedId)
    {
        return (new Feed)->findOne($feedId);
    }

    /**
     * 获取用户的推送
     * @param $uid
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    public function getUserFeed($uid, $page = 1, $limit = 10)
    {
        //数据库读取
        return FeedLog::find([
            'order' => ['id', 'desc'],
            'uid' => $uid,
            'limit' => $limit,
            'offset' => ($page - 1) * $limit,
        ])->field(['dynamic_id'])->column('dynamic_id');
    }

    /**
     * 获取用户的队列推送
     * @param $uid
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    public function pullUserFeed($uid, $limit = 10)
    {
        $dynamicIds = $this->getRedis()->lrange("feed-user:" . $uid, ($page - 1) * $limit, $limit);
        //todo 持久化存储推送记录
        return $dynamicIds;
    }

    /**
     * 增加feed信息
     * @param $id
     * @param $uid
     * @return Feed
     * @throws FeedException
     */
    public function createFeedByDynamic($id, $uid)
    {
        $feed = new Feed();
        $rs = $feed->save([
            'dynamic_id' => $id,
            'uid' => $uid,
        ]);
        if (!$rs) {
            throw new FeedException(
                FeedException::$addFail[0],
                FeedException::$addFail[1]
            );
        }
        return $feed;
    }

}