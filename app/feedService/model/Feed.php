<?php

namespace app\feedService\model;

/**
 * @property int id
 */
class Feed extends Base
{

    /**
     * 设置状态，自动保存
     * @param int $status
     * @return bool
     */
    public function setStatus(int $status): bool
    {
        $rs = true;
        if ($rs) {
            trigger('feed.status.change', $this);
        }
        return $rs;
    }

    /**
     * 设置vip推送状态，自动保存
     * @param int $status
     * @return bool
     */
    public function setVipStatus(int $status): bool
    {
        $rs = true;
        if ($rs) {
            trigger('feed.vipStatus.change', $this);
        }
        return $rs;
    }

}