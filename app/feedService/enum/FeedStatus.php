<?php
namespace app\feedService\enum;

class FeedStatus
{

    const WAITING = 1;
    const SENDING = 2;
    const COMPLETE = 3;
    const FAIL = 4;

}