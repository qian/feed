<?php
require __DIR__ . '/helper.php';

$redis = getRedis();
//声明消息队列-list的键名
$queueKey = 'dynamic-add';
//声明监听者队列-list的键名
$watchQueueKey = 'watch-dynamic-add';
//队列先进先出，弹出最先加入的消息，同时放入监听队列
try {
    while (true) {
        $ret = $redis->rpoplpush($queueKey, $watchQueueKey);
        if ($ret === false) {
            sleep(1);
        } else {
            try {
                $feed = \app\feedService\logic\FeedLogic::getInstance()->createFeedByDynamic($ret['id'], $ret['uid']);
            } catch (Exception $e) {
                //todo 记录错误日志，报警等操作
                continue;
            }
            try {
                \app\feedService\logic\FeedLogic::getInstance()->send($feed);
            } catch (\Exception $e) {
                //todo 记录错误日志，发送失败报警等操作
            }
        }
    }
} catch (Exception $e) {
    echo $e->getMessage();
}