<?php

namespace app\feedService\exceptions;

class FeedException extends \Exception
{
    static $notFound = [40004, 'feed不存在'];
    static $addFail = [40001, 'feed增加失败'];
    static $pushVipHasError = [40002, 'vip推送有错误'];
}