<?php

namespace app\feeService\rpcService\service;

use app\feedService\logic\FeedLogic;
use app\feeService\rpcService\FeedInterface;

class FeedService implements FeedInterface
{

    /**
     * 增加一条推送
     * @param int $dynamicId
     * @return bool
     */
    public function add(int $dynamicId): bool
    {
        return true;
    }

    /**
     * 获取用户推送
     * @param string $uid
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function get(string $uid, $page = 1, $limit = 10): array
    {
        return FeedLogic::getInstance()->getUserFeed($uid, $page, $limit);
    }

    /**
     * 从队列拉去
     * @param string $uid
     * @param int $limit
     * @return
     */
    public function pull(string $uid, $limit = 10)
    {
        return FeedLogic::getInstance()->pullUserFeed($uid, $limit);
    }
}